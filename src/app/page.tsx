'use client'

import axios from "axios";
import { useEffect, useState } from "react";

export default function Home() {
  const [ userlist , setUserList ] = useState<any[]>([]);

  useEffect(() => {
    axios.get("https://vwfuwnyjjc.execute-api.eu-west-3.amazonaws.com/dev/media")
    .then(res => {
      setUserList(res.data)
    }).catch(err => {
      console.error(err);
    })
  } , [])
  
  return (
    <main className="flex min-h-screen flex-col  p-24">
      {
        userlist.map((data , index) => (
          <h1 key={index}> full_name : { data?.full_name }   ----------------- with username :{data?.username} </h1>
        ))
      }
    </main>
  );
}
